# Confluence User Macros

## Show Only to Admins

A macro that renders body content to page (html or other macros) only if the user has Administration Privileges
in the space of the page.

> [show_only_admins](show_only_admins.html)

## Checkbox and dropdown selector label controler macro

Two macros that set or unset Confluence labels on a page.

> - [Dropdown label controler](dropdown_label.html)
> - [Checkbox label controler](dropdown_label.html)